<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.cpp" line="139"/>
        <location filename="mainwindow.cpp" line="159"/>
        <source>Initialization error</source>
        <translation>Errore di inizializzazione</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="140"/>
        <source>Could not read a colour theme: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="144"/>
        <source>light</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="145"/>
        <source>dark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="146"/>
        <source>B16 light</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="147"/>
        <source>B16 dark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="160"/>
        <source>Could not find syntax definitions. Check installation.</source>
        <translation>Non ho trovato definizioni sintattiche. Controlla l&apos;installazione.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="241"/>
        <source>Always at your service</source>
        <translation>Sempre a tua disposizione</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="277"/>
        <source>Select one or more files to open</source>
        <translation>Seleziona uno o più file da aprire</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="314"/>
        <source>Select destination directory</source>
        <translation>Seleziona la cartella di destinazione</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="971"/>
        <location filename="mainwindow.cpp" line="1002"/>
        <source>Output error</source>
        <translation>Errore di output</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="971"/>
        <source>Output directory does not exist!</source>
        <translation>Cartella di destinazione inesistente!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1002"/>
        <source>You must define a style output file!</source>
        <translation>Devi definire un file di destinazione per lo stile!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1051"/>
        <source>Processing %1 (%2/%3)</source>
        <translation>Sto processando %1 (%2/%3)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1061"/>
        <source>Language definition error</source>
        <translation>Errore nella definizione del linguaggio</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1062"/>
        <source>Invalid regular expression in %1:
%2</source>
        <translation>Espressione regolare non valida in %1:
%2</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1066"/>
        <source>Unknown syntax</source>
        <translation>Sintassi sconosciuta</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1066"/>
        <source>Could not convert %1</source>
        <translation>Impossibile convertire %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1069"/>
        <source>Lua error</source>
        <translation>Errore Lua</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1069"/>
        <source>Could not convert %1:
Lua Syntax error: %2</source>
        <translation>Impossibile convertire %1:
Errore di sintassi Lua: %2</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1213"/>
        <source>Converted %1 files in %2 ms</source>
        <translation>Convertiti %1 file in %2 ms</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1398"/>
        <source>Conversion of &quot;%1&quot; not possible.</source>
        <translation>Impossibile convertire &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1398"/>
        <location filename="mainwindow.cpp" line="1539"/>
        <location filename="mainwindow.cpp" line="1582"/>
        <source>clipboard data</source>
        <translation>dati negli appunti</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1469"/>
        <source>%1 options</source>
        <translation>opzioni %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1512"/>
        <location filename="mainwindow.cpp" line="1523"/>
        <source>(user script)</source>
        <translation>(script utente)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1538"/>
        <source>Preview (%1):</source>
        <translation>Anteprima (%1):</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1541"/>
        <source>Current syntax: %1 %2</source>
        <translation>Sintassi in uso: %1 %2</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1542"/>
        <source>Current theme: %1 %2</source>
        <translation>Tema in uso: %1 %2</translation>
    </message>
    <message>
        <source>Current syntax: %1</source>
        <translation type="vanished">Sintassi in uso: %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1582"/>
        <source>Preview of &quot;%1&quot; not possible.</source>
        <translation>Impossibile mostrare l&apos;anteprima di &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1622"/>
        <location filename="mainwindow.cpp" line="1626"/>
        <location filename="mainwindow.cpp" line="1630"/>
        <location filename="mainwindow.cpp" line="1635"/>
        <source>Choose a style include file</source>
        <translation>Scegli il file di stile da includere</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1640"/>
        <source>About providing translations</source>
        <translation>Come contribuire alla traduzione</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1641"/>
        <source>The GUI was developed using the Qt toolkit, and translations may be provided using the tools Qt Linguist and lrelease.
The highlight.ts file for Linguist resides in the src/gui-qt subdirectory.
The qm file generated by lrelease has to be saved in gui-files/l10n.

Please send a note to as (at) andre-simon (dot) de if you have issues during translating or if you have finished or updated a translation.</source>
        <translation>L&apos;interfaccia grafica è sviluppata con Qt toolkit, per tradurla in altre lingue servono Qt Linguist e lrelease.
Il file &quot;highlight.ts&quot; per Qt Linguist si trova nella sottocartella &quot;&apos;src/gui-qt&quot;.
Il file qm generato da lrelease dovrà essere salvato in &quot;gui-files/l10n&quot;.

In caso di difficoltà, o se hai completato o aggiornato una traduzione, contattami tramite posta elettronica: as (at) andre-simon (dot) de.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1699"/>
        <source>Select one or more plug-ins</source>
        <translation>Seleziona uno o più plug-in</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1710"/>
        <source>Select one or more syntax or theme scripts</source>
        <translation>Scegli uno o più file di sintassi o tema</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1770"/>
        <source>Choose a plug-in input file</source>
        <translatorcomment>( TIP ) Plug-ins -&gt; Plug-in parameter -&gt; [...]</translatorcomment>
        <translation>Scegli il file di input per il plug-in</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1828"/>
        <source>Some plug-in effects may not be visible in the preview.</source>
        <translation>Alcuni effetti dei plug-in potrebbero non essere visualizzati nell&apos;anteprima.</translation>
    </message>
</context>
<context>
    <name>MainWindowClass</name>
    <message>
        <location filename="mainwindow.ui" line="17"/>
        <source>Highlight</source>
        <translation>Highlight</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="30"/>
        <source>Preview</source>
        <translation>Antemprima</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="58"/>
        <source>Output progress:</source>
        <translation>Progresso output:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="86"/>
        <source>&amp;File</source>
        <translation>&amp;File</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="94"/>
        <source>&amp;Help</source>
        <translation>&amp;Aiuto</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="110"/>
        <source>Wi&amp;ndows</source>
        <translation>Fi&amp;nestre</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="130"/>
        <source>H&amp;ighlighting options</source>
        <oldsource>High&amp;lighting options</oldsource>
        <translation>Opzioni di &amp;evidenziatura codice</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="175"/>
        <source>Files</source>
        <translation>File</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="181"/>
        <source>Choose the source code files you want to convert.</source>
        <translatorcomment>( TIP )</translatorcomment>
        <translation>Scegli i file sorgente da convertire.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="187"/>
        <source>Choose input files</source>
        <translation>Scegli i file di input</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="213"/>
        <source>List of input files.</source>
        <translatorcomment>( TIP )</translatorcomment>
        <translation>Elenco dei file di input.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="237"/>
        <source>Remove the selected input files.</source>
        <translatorcomment>( TIP )</translatorcomment>
        <translation>Rimuovi dall&apos;elenco i file di input selezionati.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="240"/>
        <location filename="mainwindow.ui" line="583"/>
        <location filename="mainwindow.ui" line="719"/>
        <source>Clear selection</source>
        <translation>Rimuovi selezione</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="247"/>
        <source>Remove all input files.</source>
        <translatorcomment>( TIP )</translatorcomment>
        <translation>Rimuovi dall&apos;elenco tutti i file di input.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="250"/>
        <location filename="mainwindow.ui" line="593"/>
        <location filename="mainwindow.ui" line="729"/>
        <source>Clear all</source>
        <translation>Rimuovi tutto</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="274"/>
        <source>Output destination:</source>
        <translation>Destinazione output:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="289"/>
        <source>Output directory</source>
        <translatorcomment>( TIP )</translatorcomment>
        <translation>Cartella di destinazione</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="308"/>
        <source>Select the output directory.</source>
        <translatorcomment>( TIP )</translatorcomment>
        <translation>Scegli la cartella di destinazione.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="311"/>
        <location filename="mainwindow.ui" line="670"/>
        <location filename="mainwindow.ui" line="1334"/>
        <location filename="mainwindow.ui" line="1603"/>
        <location filename="mainwindow.ui" line="1711"/>
        <location filename="mainwindow.ui" line="2006"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="346"/>
        <source>Save output in the input file directories.</source>
        <translatorcomment>( TIP )</translatorcomment>
        <translation>Salva l&apos;output nelle cartelle dei file di input.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="349"/>
        <source>Write to source directories</source>
        <translation>Scrivi nelle cartelle dei sorgenti</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="384"/>
        <source>Start the conversion of your input files.</source>
        <translatorcomment>( TIP )</translatorcomment>
        <translation>Avvia la conversione dei file di input.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="387"/>
        <source>Convert files</source>
        <translation>Converti file</translation>
    </message>
    <message>
        <source>Copy highlighted code of the seleted file into the clipboard.</source>
        <translatorcomment>( TIP )</translatorcomment>
        <translation type="vanished">Copia negli appunti il codice con sintassi evidenziata del file selezionato.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="400"/>
        <source>Copy file to clipboard</source>
        <translation>Copia file negli appunti</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="412"/>
        <source>Clipboard</source>
        <translation>Appunti</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="436"/>
        <source>Paste clipboard content into the preview window.</source>
        <translatorcomment>( TIP )</translatorcomment>
        <translation>Incolla il contenuto degli appunti nella finestra di anteprima.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="439"/>
        <source>Paste from clipboard (%1)</source>
        <translation>Incolla appunti (%1)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="420"/>
        <source>Select syntax:</source>
        <translation>Scegli la sintassi:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="330"/>
        <source>Browse output directory</source>
        <translation>Apri la cartella di destinazione</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="397"/>
        <source>Copy highlighted code of the selected file into the clipboard.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="427"/>
        <source>Select the correct syntax of the code snippet.</source>
        <translatorcomment>( TIP )</translatorcomment>
        <translation>Scegli la sintassi appropriata per il frammento di codice.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="456"/>
        <source>Copy highlighted code into the clipboard.</source>
        <translatorcomment>( TIP )</translatorcomment>
        <translation>Copia negli appunti il codice con sintassi evidenziata.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="459"/>
        <source>Copy preview to clipboard (%1)</source>
        <translation>Copia negli appunti l&apos;anteprima (%1)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="482"/>
        <source>Paste code from clipboard and copy the output back in one step</source>
        <translation>Incolla il codice dagli appunti e copiavi il risultato in un solo passaggio</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="488"/>
        <source>Paste, Convert and Copy</source>
        <translation>Incolla, Converti e Copia</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="514"/>
        <source>Only output the selected lines of the preview.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="517"/>
        <source>Output selected lines only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="542"/>
        <source>Plug-ins</source>
        <translation>Plug-in</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="548"/>
        <source>Choose a plug-in script.</source>
        <translatorcomment>( TIP )</translatorcomment>
        <translation>Scegli uno o più script di plug-in da aggiungere all&apos;elenco.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="551"/>
        <source>Add plug-in to pool</source>
        <translation>Aggiungi plug-in</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="562"/>
        <source>List of plug-ins. Toggle checkbox to enable the scripts. The preview window may not display all plug-in effects.</source>
        <translatorcomment>( TIP )</translatorcomment>
        <translation>Elenco dei plug-in. Spunta la casella di un plug-in per attivarlo
Nota: La finestra di anteprima potrebbe non mostrare alcuni effetti dei plug-in.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="580"/>
        <source>Remove the selected plug-ins.</source>
        <translatorcomment>( TIP )</translatorcomment>
        <translation>Rimuovi dall&apos;elenco i plug-in selezionati.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="590"/>
        <source>Remove all plug-ins.</source>
        <translation>Rimuovi dall&apos;elenco tutti i plug-in.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="602"/>
        <location filename="mainwindow.ui" line="738"/>
        <source>Description</source>
        <translation>Descrizione</translation>
    </message>
    <message>
        <source>Shows decription of a selected plug-in script.</source>
        <translatorcomment>( TIP )</translatorcomment>
        <translation type="vanished">Mostra la descrizione del plug-in selezionato.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="617"/>
        <source>Plug-In description</source>
        <translation>Descrizione del plug-in</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="645"/>
        <source>Plug-in parameter</source>
        <translation>Parametro del plug-in</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="654"/>
        <source>Optional plug-in parameter, this may be a path to a plug-in input file</source>
        <translatorcomment>( TIP )</translatorcomment>
        <translation>Parametro opzionale passato al plug-in, può anche essere il percorso di in file di input</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="667"/>
        <source>Select the plug-in input file.</source>
        <translation>Scegli il file di input per il plug-in.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="684"/>
        <source>Scripts</source>
        <translation>Script</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="690"/>
        <source>Choose a custom syntax or theme script.</source>
        <translation>Usa un file di sintassi o un tema personalizzati.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="693"/>
        <source>Add custom script to pool</source>
        <translation>Aggiungi script personalizzati</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="707"/>
        <source>You can check one file of each type (syntax and theme) to override the default behaviour. If checked, the scripts will be watched for changes.</source>
        <oldsource>You can check one file of each type (syntax and theme) to override the default behaviour.</oldsource>
        <translation>Puoi selezionare un file di ciascun tipo (sintassi e tema) per sovrascrivere il comportamento predefinito. Gli script selezionati saranno monitorati per rilevare cambiamenti nei file.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="716"/>
        <source>Remove the selected scripts.</source>
        <translation>Rimuovi dall&apos;elenco gli script selezionati.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="726"/>
        <source>Remove all scripts.</source>
        <translation>Rimuovi dall&apos;elenco tutti gli script.</translation>
    </message>
    <message>
        <source>Shows decription of a selected script.</source>
        <translation type="vanished">Mostra la descrizione dello script selezionato.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="747"/>
        <source>Script description</source>
        <translation>Descrizione dello script</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="789"/>
        <source>General</source>
        <translation>Generale</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="809"/>
        <source>Output for&amp;mat:</source>
        <translation>For&amp;mato output:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="831"/>
        <source>Choose an output format.</source>
        <translatorcomment>( TIP )</translatorcomment>
        <translation>Scegli il formato per l&apos;output.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="835"/>
        <source>HTML</source>
        <translation>HTML</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="840"/>
        <source>XHTML</source>
        <translation>XHTML</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="845"/>
        <source>LaTeX</source>
        <translation>LaTeX</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="850"/>
        <source>TeX</source>
        <translation>TeX</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="855"/>
        <source>RTF</source>
        <translation>RTF</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="860"/>
        <source>ODT</source>
        <translation>ODT</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="865"/>
        <source>SVG</source>
        <translation>SVG</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="870"/>
        <source>BBCode</source>
        <translation>BBCode</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="904"/>
        <source>Add line numbers to the output.</source>
        <translatorcomment>( TIP )</translatorcomment>
        <translation>Aggiungi numeri di riga nell&apos;output.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="907"/>
        <source>Add line numbers</source>
        <translation>Aggiungi numeri di riga</translation>
    </message>
    <message>
        <source>Set line numbering start</source>
        <translatorcomment>( TIP )</translatorcomment>
        <translation type="vanished">Imposta valore iniziale dei numeri di riga</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="946"/>
        <source>Select the line number width.</source>
        <translatorcomment>( TIP )</translatorcomment>
        <translation>Imposta la larghezza (in caratteri) dei numeri di riga.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="967"/>
        <source>Fill leading space of line numbers with zeroes.</source>
        <translatorcomment>( TIP )</translatorcomment>
        <translation>Riempi con zeri lo spazio vuoto che precede i numeri di riga.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="970"/>
        <source>Pad with zeroes</source>
        <translation>Riempi con zeri</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="983"/>
        <source>Generate output without document header and footer.</source>
        <translatorcomment>( TIP )</translatorcomment>
        <translation>Genera documenti senza intestazione e pié di pagina.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="986"/>
        <source>Omit header and footer</source>
        <translation>Ometti intestazione e pié di pagina</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="993"/>
        <source>Output any plug-in text injections even if document header and footer is omitted.</source>
        <translatorcomment>( TIP )</translatorcomment>
        <translation>Preserva nell&apos;output qualsiasi testo iniettato dai plug-in, anche quando  intestazione e pié di pagina sono omessi.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="996"/>
        <source>Keep Plug-In injections</source>
        <translation>Preserva iniezioni dei plug-in</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1003"/>
        <source>Test if input data is not binary.
Removes Unicode BOM mark.</source>
        <translatorcomment>( TIP )</translatorcomment>
        <translation>Testa che i dati di input non siano binari.
Rimuovi la sequenza BOM Unicode.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1007"/>
        <source>Validate input data</source>
        <translation>Valida i dati di input</translation>
    </message>
    <message>
        <source>Set the output file ancoding.</source>
        <translation type="vanished">Imposta la codifica di caratteri per l&apos;output.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="614"/>
        <source>Shows description of a selected plug-in script.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="744"/>
        <source>Shows description of a selected script.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="875"/>
        <source>ANSI ESC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="880"/>
        <source>XTerm256 ESC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="885"/>
        <source>Truecolor ESC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="927"/>
        <source>Set line numbering start.</source>
        <translatorcomment>( TIP )</translatorcomment>
        <translation>Imposta valore iniziale dei numeri di riga.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1014"/>
        <source>Generate output without version information comment.</source>
        <translation>Genera l&apos;output senza i commenti informativi sulla versione.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1017"/>
        <source>Omit version info comment</source>
        <translation>Ometti commenti di versione</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1026"/>
        <source>Set the output file encoding.</source>
        <translation>Imposta la codifica caratteri del file di output.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1029"/>
        <source>Set encoding:</source>
        <translation>Codifica di caratteri:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1036"/>
        <source>Select or define the encoding.
The result has to match the input file encoding.</source>
        <translation>Seleziona o definisci la codifica di caratteri.
Il risultato deve combaciare con la codifica di caratteri del file di input.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1047"/>
        <source>ISO-8859-1</source>
        <translation>ISO-8859-1</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1052"/>
        <source>ISO-8859-2</source>
        <translation>ISO-8859-2</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1057"/>
        <source>ISO-8859-3</source>
        <translation>ISO-8859-3</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1062"/>
        <source>ISO-8859-4</source>
        <translation>ISO-8859-4</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1067"/>
        <source>ISO-8859-5</source>
        <translation>ISO-8859-5</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1072"/>
        <source>ISO-8859-6</source>
        <translation>ISO-8859-6</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1077"/>
        <source>ISO-8859-7</source>
        <translation>ISO-8859-7</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1082"/>
        <source>ISO-8859-8</source>
        <translation>ISO-8859-8</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1087"/>
        <source>ISO-8859-9</source>
        <translation>ISO-8859-9</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1092"/>
        <source>ISO-8859-10</source>
        <translation>ISO-8859-10</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1097"/>
        <source>ISO-8859-11</source>
        <translation>ISO-8859-11</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1102"/>
        <source>ISO-8859-12</source>
        <translation>ISO-8859-12</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1107"/>
        <source>ISO-8859-13</source>
        <translation>ISO-8859-13</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1112"/>
        <source>ISO-8859-14</source>
        <translation>ISO-8859-14</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1117"/>
        <source>ISO-8859-15</source>
        <translation>ISO-8859-15</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1122"/>
        <source>UTF-8</source>
        <translation>UTF-8</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1146"/>
        <source>Output specific</source>
        <translation>Specifiche output</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1187"/>
        <source>Stylesheets</source>
        <translation>Fogli di stile</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1195"/>
        <location filename="mainwindow.ui" line="1521"/>
        <location filename="mainwindow.ui" line="1629"/>
        <location filename="mainwindow.ui" line="1924"/>
        <source>Include the style information in each output file.</source>
        <translatorcomment>( TIP for HTML/XML )</translatorcomment>
        <translation>Includi le informazioni di stile in ciascun file di output.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1198"/>
        <location filename="mainwindow.ui" line="1927"/>
        <source>Embed style (CSS)</source>
        <translation>Incorpora stile (CSS)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1211"/>
        <source>Add CSS information to each tag (do not use CSS class definitions).</source>
        <translatorcomment>( TIP for HTML/XML )</translatorcomment>
        <translation>Aggiungi  le informazioni di stile in ciascun tag (non usare classi CSS).</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1214"/>
        <source>Inline CSS</source>
        <translation>CSS in linea</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1228"/>
        <source>CSS class prefix:</source>
        <translation>Prefisso classi CSS:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1241"/>
        <source>Add a CSS class name prefix to avoid namespace clashes.</source>
        <translatorcomment>( TIP for HTML/XML )</translatorcomment>
        <translation>Aggiungi un prefisso ai nomi delle classi CSS per evitare conflitti di namespace.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1255"/>
        <location filename="mainwindow.ui" line="1533"/>
        <location filename="mainwindow.ui" line="1641"/>
        <location filename="mainwindow.ui" line="1936"/>
        <source>Stylesheet file:</source>
        <translation>File del foglio di stile:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1274"/>
        <location filename="mainwindow.ui" line="1657"/>
        <location filename="mainwindow.ui" line="1952"/>
        <source>Name of the referenced style file.</source>
        <translatorcomment>( TIP for HTML/XML )</translatorcomment>
        <translation>Nome da dare al file del foglio di stile di riferimento.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1277"/>
        <location filename="mainwindow.ui" line="1955"/>
        <source>highlight.css</source>
        <translation>highlight.css</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1288"/>
        <location filename="mainwindow.ui" line="1563"/>
        <location filename="mainwindow.ui" line="1671"/>
        <location filename="mainwindow.ui" line="1966"/>
        <source>Stylesheet include file:</source>
        <translation>Foglio di stile da includere:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1318"/>
        <location filename="mainwindow.ui" line="1990"/>
        <source>Path of the CSS include file.</source>
        <translatorcomment>( TIP for HTML/XML )</translatorcomment>
        <translation>Percorso del file CSS da includere.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1331"/>
        <source>Select a CSS include file.</source>
        <translatorcomment>( TIP for HTML/XML )</translatorcomment>
        <translation>Scegli un file CSS da includere.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1360"/>
        <source>Misc</source>
        <translation>Varie</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1372"/>
        <source>Line numbering options:</source>
        <translation>Opzioni numeri di riga:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1379"/>
        <source>Add an anchor to each line.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2486"/>
        <source>&amp;File access trace (W32)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add an achor to each line.</source>
        <translatorcomment>( TIP for HTML/XML )</translatorcomment>
        <translation type="vanished">Aggiungi un ancora a ciascuna riga.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1382"/>
        <source>Add line anchors</source>
        <translation>Ancora le righe</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1389"/>
        <source>Add the filename as prefix to the anchors.</source>
        <translatorcomment>( TIP for HTML/XML )</translatorcomment>
        <translation>Prefiggi il nome del file alle ancore.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1392"/>
        <source>Include file name in anchor</source>
        <translation>Includi nome file nelle ancore</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1399"/>
        <source>Output the lines within an ordered list.</source>
        <translatorcomment>( TIP for HTML/XML )</translatorcomment>
        <translation>Genera i numeri di riga tramite un elenco numerato.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1402"/>
        <source>Output as ordered list</source>
        <translation>Genera elenco numerato</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1416"/>
        <source>Generate an index file with hyperlinks to all outputted files.</source>
        <translatorcomment>( TIP for HTML/XML )</translatorcomment>
        <translation>Crea un file indice con link ipertestuali a tutti i file generati.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1419"/>
        <source>Generate index file</source>
        <translation>Crea file indice</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1426"/>
        <source>Add &amp;lt;pre&amp;gt; tags to the output, if the flag &quot;No document header and footer&quot; is selected.</source>
        <translatorcomment>( TIP for HTML/XML )</translatorcomment>
        <translation>Aggiungi nel file di output i tag &amp;lt;pre&amp;gt; (se l&apos;opzione &quot;Ometti intestazione e pié di pagina&quot; è attiva).</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1429"/>
        <source>Enclose in pre tags</source>
        <translation>Racchiudi in tag &lt;pre&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1443"/>
        <source>Add HTML MIME Type when copying code to the clipboard</source>
        <translation>Aggiungi lo HTML MIME Type al codice copiato negli appunti</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1446"/>
        <source>Copy with MIME type</source>
        <translation>Copia con MIME Type</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1474"/>
        <source>Replace quotes by dq sequences.</source>
        <translatorcomment>( TIP for LaTeX ) --- Non sono sicuro della traduzione perché non mi è chiaro il contesto.</translatorcomment>
        <translation>Sostituisci le virgolette con sequenze dq.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1477"/>
        <source>Escape quotes</source>
        <translation>Escaping delle virgolette</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1484"/>
        <source>Adapt output for the Babel package</source>
        <translation>Adatta l&apos;output al pacchetto Babel</translation>
    </message>
    <message>
        <source>Toggle classic theme or Base16 theme selection.</source>
        <translation type="vanished">Alterna la selezione tra temi classici e temi Base16.</translation>
    </message>
    <message>
        <source>Base16</source>
        <translation type="vanished">Base16</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2370"/>
        <source>Font si&amp;ze:</source>
        <translation>&amp;Dimensione carattere:</translation>
    </message>
    <message>
        <source>Make output Babel compatible.</source>
        <translatorcomment>( TIP for LaTeX )</translatorcomment>
        <translation type="vanished">Rendi l&apos;output compatibile con Babel.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1487"/>
        <source>Add Babel compatibility</source>
        <translation>Compatibilità con Babel</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1494"/>
        <source>Adapt output for the Beamer package</source>
        <translation>Adatta l&apos;output al pacchetto Beamer</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1497"/>
        <source>Add Beamer compatibility</source>
        <translation>Aggiungi compatibilità Beamer</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1504"/>
        <source>Replace default symbols (brackets, tilde) by nice redefinitions.</source>
        <translatorcomment>( TIP for LaTeX )</translatorcomment>
        <translation>Sostituisci i simboli predefiniti (parentesi, tilde) con ridefinizioni più attraenti.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1507"/>
        <source>Add pretty symbols</source>
        <translation>Usa simboli eleganti</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1524"/>
        <source>Embed style (defs)</source>
        <translatorcomment>( LaTeX option ) NOTA: Non sono certo se&quot;definizioni&quot; vada bene.</translatorcomment>
        <translation>Incorpora stili (definizioni)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1549"/>
        <source>Name of the referenced  style file.</source>
        <translation>Nome da dare al file di stile di riferimento.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1552"/>
        <location filename="mainwindow.ui" line="1660"/>
        <source>highlight.sty</source>
        <translation>highlight.sty</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1587"/>
        <location filename="mainwindow.ui" line="1695"/>
        <source>Path of the style include file.</source>
        <translatorcomment>( TIP for LaTeX )</translatorcomment>
        <translation>Percorso del file di stile da includere.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1600"/>
        <location filename="mainwindow.ui" line="1708"/>
        <location filename="mainwindow.ui" line="2003"/>
        <source>Select a style include file.</source>
        <translatorcomment>( TIP for LaTeX )</translatorcomment>
        <translation>Scegli il file di stile da includere.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1632"/>
        <source>Embed style (macros)</source>
        <translatorcomment>( LaTeX option )</translatorcomment>
        <translation>Incorpora stile (macro)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1737"/>
        <source>Add character stylesheets with formatting information.
You can select the stylesheets in your word processor to reformat additional text.</source>
        <translatorcomment>( TIP for RTF )</translatorcomment>
        <translation>Aggiungi fogli di stile dei caratteri con informazioni sulla formattazione.
Puoi selezionare i fogli di stile nel tuo word processor per riformattare testo aggiuntivo.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1741"/>
        <source>Add character styles</source>
        <translatorcomment>( RTF options )</translatorcomment>
        <translation>Aggiungi stili dei caratteri</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1748"/>
        <source>Set page color attribute to background color.</source>
        <translatorcomment>Tip for RTF</translatorcomment>
        <translation>Imposta l&apos;attributo colore della pagina al colore dello sfondo.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2150"/>
        <source>Reformat and indent your code.
This feature is enabled for C, C++, C# and Java code.</source>
        <translation>Riformatta e indenta il codice sorgente.
Quest&apos;opzione è disponibile per i linguaggi C, C++, C# e Java.</translation>
    </message>
    <message>
        <source>Set page color attribute to background color</source>
        <translatorcomment>( TIP for RTF )</translatorcomment>
        <translation type="vanished">Imposta l&apos;attributo colore della pagina al colore dello sfondo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1751"/>
        <source>Set page color</source>
        <translatorcomment>( RTF options )</translatorcomment>
        <translation>Imposta colore pagina</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1760"/>
        <source>Page size:</source>
        <translatorcomment>( RTF options )</translatorcomment>
        <translation>Dimensioni pagina:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1773"/>
        <source>Select a page size.</source>
        <translatorcomment>( TIP for RTF )</translatorcomment>
        <translation>Seleziona una formato per le dimensioni della pagina.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1780"/>
        <source>A3</source>
        <translation>A3</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1785"/>
        <source>A4</source>
        <translation>A4</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1790"/>
        <source>A5</source>
        <translation>A5</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1795"/>
        <source>B4</source>
        <translation>B4</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1800"/>
        <source>B5</source>
        <translation>B5</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1805"/>
        <source>B6</source>
        <translation>B6</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1810"/>
        <source>Letter</source>
        <translation>Lettera</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1815"/>
        <source>Legal</source>
        <translation>Legale</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1844"/>
        <source>I&amp;mage width:</source>
        <translatorcomment>( SVG option )</translatorcomment>
        <translation>Larghezza i&amp;mmagine:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1872"/>
        <source>Enter the SVG width (may contain units).</source>
        <translatorcomment>( TIP for SVG )</translatorcomment>
        <translation>Inserisci la larghezza SVG (può contenere unità).</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1892"/>
        <source>Height:</source>
        <translatorcomment>( SVG option )</translatorcomment>
        <translation>Altezza:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1908"/>
        <source>Enter the SVG height (may contain units).</source>
        <translatorcomment>( TIP for SVG )</translatorcomment>
        <translation>Inserisci l&apos;altezza SVG (può contenere unità).</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2032"/>
        <source>No options defined.</source>
        <translatorcomment>( ODT / BBcode options )</translatorcomment>
        <translation>Nessun&apos;opzione disponibile.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2057"/>
        <source>Formatting</source>
        <translation>Formattazione</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2086"/>
        <source>Color theme:</source>
        <translation>Tema:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2133"/>
        <source>Select a colour theme.</source>
        <translatorcomment>( TIP )</translatorcomment>
        <translation>Seleziona un tema di colori.</translation>
    </message>
    <message>
        <source>Reformat and indent your code.
This feature is enabled tor C, C++, C# and Java code.</source>
        <translatorcomment>( TIP )</translatorcomment>
        <translation type="vanished">Riformatta e indenta il codice sorgente.
Quest&apos;opzione è disponibile per i linguaggi C, C++, C# e Java.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2154"/>
        <source>Reformat:</source>
        <translation>Riformatta:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2173"/>
        <source>Choose a formatting scheme.</source>
        <translatorcomment>( TIP )</translatorcomment>
        <translation>Scegli uno schema di formattazione.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2187"/>
        <source>Change the keyword case.</source>
        <translatorcomment>( TIP )</translatorcomment>
        <translation>Scegli il criterio di maiuscolo/minuscolo con cui riformattare le keyword.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2190"/>
        <source>Keyword case:</source>
        <translatorcomment>NOTA: letter case è intraducibile, keyword ha più senso non tradurlo.</translatorcomment>
        <translation>Keyword:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2209"/>
        <source>Select a keyword case.</source>
        <translatorcomment>( TIP )</translatorcomment>
        <translation>Scegli maiuscolo/minuscolo.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2216"/>
        <source>UPPER</source>
        <translation>MAIUSCOLO</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2221"/>
        <source>lower</source>
        <translation>minuscolo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2226"/>
        <source>Capitalize</source>
        <translation>Iniziali Maiuscole</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2244"/>
        <source>Tab width:</source>
        <translation>Tabulazione:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2260"/>
        <source>Enter the number of spaces which replace a tab.
Set the width to 0 to keep tabs.</source>
        <translatorcomment>( TIP )</translatorcomment>
        <translation>Imposta il numero di spazi con cui rimpiazzare una tabulazione.
Mettere 0 per preservare le tabulazioni.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2275"/>
        <source>Enable line wrapping.</source>
        <translatorcomment>( TIP )</translatorcomment>
        <translation>Abilita la funzione di a capo automatico.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2278"/>
        <source>Line wrapping</source>
        <translation>A capo automatico</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2291"/>
        <source>Enter the maximum line length.</source>
        <translatorcomment>( TIP )</translatorcomment>
        <translation>Inserisci la lunghezza massima di riga (numero di caratteri).</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2310"/>
        <source>Indent statements and function parameters after wrapping.</source>
        <translatorcomment>( TIP )</translatorcomment>
        <translation>Dopo un a capo automatico, indenta istruzioni e parametri delle funzioni.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2313"/>
        <source>Intelligent wrapping</source>
        <translation>A capo automatico intelligente</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2320"/>
        <source>Do not add line numbering to lines which were automatically wrapped.</source>
        <translatorcomment>( TIP )</translatorcomment>
        <translation>Non inserire il numero di riga nelle righe su cui si è andati a capo automaticamente.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2323"/>
        <source>Omit line numbers of wrapped lines</source>
        <translation>Non numerare quando vai a capo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2334"/>
        <source>Font na&amp;me:</source>
        <translation>Tipo di ca&amp;rattere:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2355"/>
        <source>Select or enter the font name. HTML supports a list of fonts, separated with comma.</source>
        <translatorcomment>( TIP )</translatorcomment>
        <translation>Seleziona o digita il nome del font di carattere. HTML supporta un elenco di font, separati da virgole.</translation>
    </message>
    <message>
        <source>Font size:</source>
        <translation type="vanished">Dimensione carattere:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2392"/>
        <source>Enter the font size.</source>
        <translatorcomment>( TIP )</translatorcomment>
        <translation>Inserisci la dimensione del font di carattere.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2407"/>
        <source>&amp;Open files</source>
        <translation>Apri &amp;File</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2412"/>
        <source>&amp;Exit</source>
        <translation>&amp;Esci</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2417"/>
        <source>&amp;Load</source>
        <translatorcomment>NOTE: Can&apos;t find this entry in GUI for Windows!</translatorcomment>
        <translation>&amp;Apri</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2422"/>
        <source>&amp;Save</source>
        <translatorcomment>NOTE: Can&apos;t find this entry in GUI for Windows!</translatorcomment>
        <translation>&amp;Salva</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2427"/>
        <source>Load &amp;default project</source>
        <translatorcomment>NOTE: Can&apos;t find this entry in GUI for Windows!</translatorcomment>
        <translation>Apri progetto &amp;predefinito</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2432"/>
        <source>&amp;Readme</source>
        <translation>&amp;Leggimi</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2437"/>
        <source>&amp;Tips</source>
        <translatorcomment>NOTE: Can&apos;t find this entry in GUI for Windows!</translatorcomment>
        <translation>&amp;Trucchi</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2445"/>
        <source>&amp;Changelog</source>
        <translatorcomment>NOTA: Non ha senso tradurre questo termine, questo tipo di utenza lo conosce e lo usa.</translatorcomment>
        <translation>&amp;Changelog</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2450"/>
        <source>&amp;License</source>
        <translation>&amp;Licenza</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2455"/>
        <source>&amp;About Highlight</source>
        <translation>&amp;Informazioni su Highlight</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2460"/>
        <source>A&amp;bout translations</source>
        <translation>Tra&amp;durre Highlight</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2465"/>
        <source>&amp;Visit andre-simon.de</source>
        <translation>&amp;Visita andre-simon.de</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2476"/>
        <source>&amp;Dock floating panels</source>
        <translation>&amp;Parcheggia le finestre fluttuanti</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2481"/>
        <source>&amp;Plug-Ins</source>
        <translation>&amp;Plug-in</translation>
    </message>
</context>
<context>
    <name>ShowTextFile</name>
    <message>
        <location filename="showtextfile.ui" line="17"/>
        <source>Show text</source>
        <translatorcomment>NOTE: Can&apos;t find this entry in GUI for Windows!</translatorcomment>
        <translation>Mostra testo</translation>
    </message>
    <message>
        <location filename="showtextfile.ui" line="37"/>
        <source>TextLabel</source>
        <translatorcomment>NOTE: Can&apos;t find this entry in GUI for Windows!</translatorcomment>
        <translation>TextLabel</translation>
    </message>
    <message>
        <location filename="showtextfile.ui" line="71"/>
        <source>OK</source>
        <translatorcomment>NOTE: Can&apos;t find this entry in GUI for Windows!</translatorcomment>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>io_report</name>
    <message>
        <location filename="io_report.ui" line="14"/>
        <source>Error summary</source>
        <translatorcomment>NOTE: Can&apos;t find this entry in GUI for Windows!</translatorcomment>
        <translation>Riassunto degli errori</translation>
    </message>
    <message>
        <location filename="io_report.ui" line="24"/>
        <source>Input errors:</source>
        <translatorcomment>NOTE: Can&apos;t find this entry in GUI for Windows!</translatorcomment>
        <translation>Errori di input:</translation>
    </message>
    <message>
        <location filename="io_report.ui" line="34"/>
        <source>Remove files above from input file list</source>
        <translatorcomment>NOTE: Can&apos;t find this entry in GUI for Windows!</translatorcomment>
        <translation>Rimuovi i file sopraelencati dall&apos;elenco dei file di input</translation>
    </message>
    <message>
        <location filename="io_report.ui" line="48"/>
        <source>Output errors:</source>
        <translatorcomment>NOTE: Can&apos;t find this entry in GUI for Windows!</translatorcomment>
        <translation>Errori di output:</translation>
    </message>
    <message>
        <location filename="io_report.ui" line="65"/>
        <source>Reformatting not possible:</source>
        <translatorcomment>NOTE: Can&apos;t find this entry in GUI for Windows!</translatorcomment>
        <translation>Impossibile riformattare:</translation>
    </message>
    <message>
        <location filename="io_report.ui" line="82"/>
        <source>Failed syntax tests:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>syntax_chooser</name>
    <message>
        <location filename="syntax_chooser.ui" line="14"/>
        <source>Syntax selection</source>
        <translatorcomment>NOTE: Can&apos;t find this entry in GUI for Windows!</translatorcomment>
        <translation>Selezione sintassi</translation>
    </message>
    <message>
        <location filename="syntax_chooser.ui" line="32"/>
        <source>Select correct syntax</source>
        <translatorcomment>NOTE: Can&apos;t find this entry in GUI for Windows!</translatorcomment>
        <translation>Scegli la sintassi appropriata</translation>
    </message>
    <message>
        <location filename="syntax_chooser.ui" line="50"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
    <message>
        <location filename="syntax_chooser.ui" line="74"/>
        <source>These entries are configured in filetypes.conf.</source>
        <translatorcomment>( TIP) NOTE: Can&apos;t find this entry in GUI for Windows!</translatorcomment>
        <translation>Queste voci sono configurate in &quot;filetypes.conf&quot;.</translation>
    </message>
    <message>
        <location filename="syntax_chooser.ui" line="81"/>
        <source>The selection will be remembered for this session.</source>
        <translatorcomment>( TIP ) NOTE: Can&apos;t find this entry in GUI for Windows!</translatorcomment>
        <translation>Questa scelta verrà ricordata per tutta la sessione in corso.</translation>
    </message>
    <message>
        <location filename="syntax_chooser.ui" line="84"/>
        <source>Remember selection</source>
        <translation>Memorizza scelta</translation>
    </message>
    <message>
        <location filename="syntax_chooser.cpp" line="57"/>
        <source>The file extension %1 is assigned to multiple syntax definitions.
Select the correct one:</source>
        <translation>L&apos;estensione di file %1 è assegnata a più definizioni sintattiche.
Seleziona quella appropriata:</translation>
    </message>
</context>
</TS>
