<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_MX">
<context>
    <name>MainWindow</name>
    <message>
        <source>Themes or indent schemes not found.
Check installation.</source>
        <translation type="obsolete">Temas o esquemas de indentación no encontrados
Revise su instalación.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="139"/>
        <location filename="mainwindow.cpp" line="159"/>
        <source>Initialization error</source>
        <translation>Error de inicialización</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="140"/>
        <source>Could not read a colour theme: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="144"/>
        <source>light</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="145"/>
        <source>dark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="146"/>
        <source>B16 light</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="147"/>
        <source>B16 dark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="160"/>
        <source>Could not find syntax definitions. Check installation.</source>
        <translation>Definiciones de sintaxis no encontradas.
Revise su instalacion.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="241"/>
        <source>Always at your service</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="277"/>
        <source>Select one or more files to open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="314"/>
        <source>Select destination directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Tags file error</source>
        <translation type="obsolete">Error de archivo de etiquetas</translation>
    </message>
    <message>
        <source>Could not read tags information in &quot;%1&quot;</source>
        <translation type="obsolete">No se pudieron leer las etiquetas de &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="971"/>
        <location filename="mainwindow.cpp" line="1002"/>
        <source>Output error</source>
        <translation>Error de salida</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="971"/>
        <source>Output directory does not exist!</source>
        <translation>¡Directorio de salida no existe!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1002"/>
        <source>You must define a style output file!</source>
        <translation>¡Debe especificar un archivo de estilo de salida!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1051"/>
        <source>Processing %1 (%2/%3)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1061"/>
        <source>Language definition error</source>
        <translation>Error de definición de lenguaje</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1062"/>
        <source>Invalid regular expression in %1:
%2</source>
        <translation>Expresión regular inválida en %1:
%2</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1066"/>
        <source>Unknown syntax</source>
        <translation>Sintaxis desconocida</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1066"/>
        <source>Could not convert %1</source>
        <translation>No se pudo convertir %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1069"/>
        <source>Lua error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1069"/>
        <source>Could not convert %1:
Lua Syntax error: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1213"/>
        <source>Converted %1 files in %2 ms</source>
        <translation>Se convirtieron%1 archivos en %2 ms</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1398"/>
        <source>Conversion of &quot;%1&quot; not possible.</source>
        <translation>No es posible la conversión de &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1398"/>
        <location filename="mainwindow.cpp" line="1539"/>
        <location filename="mainwindow.cpp" line="1582"/>
        <source>clipboard data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1469"/>
        <source>%1 options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1512"/>
        <location filename="mainwindow.cpp" line="1523"/>
        <source>(user script)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1538"/>
        <source>Preview (%1):</source>
        <translation>Previsualización (%1):</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1541"/>
        <source>Current syntax: %1 %2</source>
        <translation>Sintaxis actual: %1 %2</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1542"/>
        <source>Current theme: %1 %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Current syntax: %1</source>
        <translation type="vanished">Sintaxis actual: %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1582"/>
        <source>Preview of &quot;%1&quot; not possible.</source>
        <translation>No es posible previsualizar &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1828"/>
        <source>Some plug-in effects may not be visible in the preview.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Choose a ctags file</source>
        <translation type="obsolete">Elija un archivo de ctags</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1622"/>
        <location filename="mainwindow.cpp" line="1626"/>
        <location filename="mainwindow.cpp" line="1630"/>
        <location filename="mainwindow.cpp" line="1635"/>
        <source>Choose a style include file</source>
        <translation>Elija un archivo de inclusión de estilo</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1640"/>
        <source>About providing translations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1641"/>
        <source>The GUI was developed using the Qt toolkit, and translations may be provided using the tools Qt Linguist and lrelease.
The highlight.ts file for Linguist resides in the src/gui-qt subdirectory.
The qm file generated by lrelease has to be saved in gui-files/l10n.

Please send a note to as (at) andre-simon (dot) de if you have issues during translating or if you have finished or updated a translation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1699"/>
        <source>Select one or more plug-ins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1710"/>
        <source>Select one or more syntax or theme scripts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1770"/>
        <source>Choose a plug-in input file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindowClass</name>
    <message>
        <location filename="mainwindow.ui" line="181"/>
        <source>Choose the source code files you want to convert.</source>
        <translation>Elijir los archivos fuente que desea convertir.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="187"/>
        <source>Choose input files</source>
        <translation>Elijir archivos de entrada</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="213"/>
        <source>List of input files.</source>
        <translation>Lista de archivos de entrada.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="237"/>
        <source>Remove the selected input files.</source>
        <translation>Remover los archivos de entrada seleccionados.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="240"/>
        <location filename="mainwindow.ui" line="583"/>
        <location filename="mainwindow.ui" line="719"/>
        <source>Clear selection</source>
        <translation>Remover seleccionados</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="247"/>
        <source>Remove all input files.</source>
        <translation>Remover todos los archivos de entrada.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="250"/>
        <location filename="mainwindow.ui" line="593"/>
        <location filename="mainwindow.ui" line="729"/>
        <source>Clear all</source>
        <translation>Remover todos</translation>
    </message>
    <message>
        <source>Output destination</source>
        <translation type="obsolete">Destino de salida</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="289"/>
        <source>Output directory</source>
        <translation>Directorio de salida</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="308"/>
        <source>Select the output directory.</source>
        <translation>Elijir el directorio de salida.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="311"/>
        <location filename="mainwindow.ui" line="670"/>
        <location filename="mainwindow.ui" line="1334"/>
        <location filename="mainwindow.ui" line="1603"/>
        <location filename="mainwindow.ui" line="1711"/>
        <location filename="mainwindow.ui" line="2006"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="346"/>
        <source>Save output in the input file directories.</source>
        <translation>Guardar salida en los directorios de entrada de cada archivo.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="349"/>
        <source>Write to source directories</source>
        <translation>Guardar a los directorios de origen</translation>
    </message>
    <message>
        <source>Output options</source>
        <translation type="obsolete">Opciones de salida</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="789"/>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="809"/>
        <source>Output for&amp;mat:</source>
        <oldsource>Output format:</oldsource>
        <translation type="unfinished">Formato de salida:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="831"/>
        <source>Choose an output format.</source>
        <translation>Elija un formato de salida.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="835"/>
        <source>HTML</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="840"/>
        <source>XHTML</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="845"/>
        <source>LaTeX</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="850"/>
        <source>TeX</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="855"/>
        <source>RTF</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="860"/>
        <source>ODT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="865"/>
        <source>SVG</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="904"/>
        <source>Add line numbers to the output.</source>
        <translation>Agregar números de linea a la salida.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="907"/>
        <source>Add line numbers</source>
        <translation>Agregar números de linea</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="946"/>
        <source>Select the line number width.</source>
        <translation>Seleccione el ancho de número de linea.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="967"/>
        <source>Fill leading space of line numbers with zeroes.</source>
        <translation>Llenar espacio antes de los números de lineas con ceros.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="970"/>
        <source>Pad with zeroes</source>
        <translation>Rellenar con ceros</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="983"/>
        <source>Generate output without document header and footer.</source>
        <translation>Generar documento de salida con cabecera y pie de página.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="986"/>
        <source>Omit header and footer</source>
        <translation>Omitir cabecera y pie de página</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1003"/>
        <source>Test if input data is not binary.
Removes Unicode BOM mark.</source>
        <translation>Probar si los datos de entrada no son binarios.
Remueve marca BOM de Unicode.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1007"/>
        <source>Validate input data</source>
        <translation>Validar datos de entrada</translation>
    </message>
    <message>
        <source>Set the output file ancoding.</source>
        <translation type="vanished">Ajuste la codificación del archivo de salida.</translation>
    </message>
    <message>
        <source>Encoding:</source>
        <translation type="obsolete">Codificación:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1036"/>
        <source>Select or define the encoding.
The result has to match the input file encoding.</source>
        <translation>Elija o defina la codificación.
El resultado tiene que corresponder a la codificación del archivo de entrada.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1047"/>
        <source>ISO-8859-1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1052"/>
        <source>ISO-8859-2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1057"/>
        <source>ISO-8859-3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1062"/>
        <source>ISO-8859-4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1067"/>
        <source>ISO-8859-5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1072"/>
        <source>ISO-8859-6</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1077"/>
        <source>ISO-8859-7</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1082"/>
        <source>ISO-8859-8</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1087"/>
        <source>ISO-8859-9</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1092"/>
        <source>ISO-8859-10</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1097"/>
        <source>ISO-8859-11</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1102"/>
        <source>ISO-8859-12</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1107"/>
        <source>ISO-8859-13</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1112"/>
        <source>ISO-8859-14</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1117"/>
        <source>ISO-8859-15</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1122"/>
        <source>UTF-8</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1146"/>
        <source>Output specific</source>
        <translation>Esoecificas de salida</translation>
    </message>
    <message>
        <source>HTML options</source>
        <translation type="obsolete">Opciones de HTML</translation>
    </message>
    <message>
        <source>Style</source>
        <translation type="obsolete">Hoja de estilo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1195"/>
        <location filename="mainwindow.ui" line="1521"/>
        <location filename="mainwindow.ui" line="1629"/>
        <location filename="mainwindow.ui" line="1924"/>
        <source>Include the style information in each output file.</source>
        <translation>Incluir la información de estilo en cada archivo de salida.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1198"/>
        <location filename="mainwindow.ui" line="1927"/>
        <source>Embed style (CSS)</source>
        <translation>Incrustar estilo (CSS)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1211"/>
        <source>Add CSS information to each tag (do not use CSS class definitions).</source>
        <translation>Agregar información de CSS a cada etiqueta (no utilizar definiciones de clase de CSS).</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1214"/>
        <source>Inline CSS</source>
        <translation></translation>
    </message>
    <message>
        <source>Style file:</source>
        <translation type="obsolete">Archivo de estilo:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1274"/>
        <location filename="mainwindow.ui" line="1657"/>
        <location filename="mainwindow.ui" line="1952"/>
        <source>Name of the referenced style file.</source>
        <translation>Nombre del archivo de estilo citado.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1277"/>
        <location filename="mainwindow.ui" line="1955"/>
        <source>highlight.css</source>
        <translation></translation>
    </message>
    <message>
        <source>Style include file:</source>
        <translation type="obsolete">Archivo de estilo a incluir:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1318"/>
        <location filename="mainwindow.ui" line="1990"/>
        <source>Path of the CSS include file.</source>
        <translation>Ruta del archivo CSS a incluir.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1331"/>
        <source>Select a CSS include file.</source>
        <translation>Seleccionar un archivo CSS a incluir.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1228"/>
        <source>CSS class prefix:</source>
        <translation>Prefijo de clase de CSS:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1241"/>
        <source>Add a CSS class name prefix to avoid namespace clashes.</source>
        <translation>Agregue un prefijo de nombre de clase de CSS para evitar conflictos.</translation>
    </message>
    <message>
        <source>Tags and index files</source>
        <translation type="obsolete">Archivos de etiquetas e índice</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1416"/>
        <source>Generate an index file with hyperlinks to all outputted files.</source>
        <translation>Generar un archivo de indice con enlaces a todos los archivos de salida.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1419"/>
        <source>Generate index file</source>
        <translation>Generar archivo de índice</translation>
    </message>
    <message>
        <source>Read a ctags file and add the included metainformation as tooltips.
See ctags.sf.net for details.</source>
        <translation type="obsolete">Leer un archivo de ctags y agregar la metainformación incluida como aviso emergente.
Vea ctags.sf.net para más detalles.</translation>
    </message>
    <message>
        <source>Ctags file:</source>
        <translation type="obsolete">Archivo de ctags:</translation>
    </message>
    <message>
        <source>Path of the ctags file.</source>
        <translation type="obsolete">Ruta del archivo de ctags.</translation>
    </message>
    <message>
        <source>Choose a ctags file.</source>
        <translation type="obsolete">Elijir un archivo de ctags.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1360"/>
        <source>Misc</source>
        <translation>Miscelaneo</translation>
    </message>
    <message>
        <source>Add an achor to each line.</source>
        <translation type="vanished">Agregar una ancla a cada linea.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1382"/>
        <source>Add line anchors</source>
        <translation>Agregar ancla por linea</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1389"/>
        <source>Add the filename as prefix to the anchors.</source>
        <translation>Agregar el nombre de archivo como prefijo a las anclas.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1392"/>
        <source>Include file name in anchor</source>
        <translation>Incluir el nombre de archivo en el ancla</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1399"/>
        <source>Output the lines within an ordered list.</source>
        <translation>Producir las lineas de salida dentro de una lista ordenada.</translation>
    </message>
    <message>
        <source>Ordered list</source>
        <translation type="obsolete">Lista ordenada</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1426"/>
        <source>Add &amp;lt;pre&amp;gt; tags to the output, if the flag &quot;No document header and footer&quot; is selected.</source>
        <translation>Agregar etiquetas &amp;lt;pre&amp;gt; a la salida, si la opción &quot;Sin cabecera ni pie de página&quot; está seleccionada.</translation>
    </message>
    <message>
        <source>Enclose in &lt;pre&gt;</source>
        <translation type="obsolete">Meter entre &amp;lt;pre&amp;gt;</translation>
    </message>
    <message>
        <source>LaTeX options</source>
        <translation type="obsolete">Opciones de LaTeX</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1474"/>
        <source>Replace quotes by dq sequences.</source>
        <translation>Reemplazar comillas por sucesiones de dobles comillas.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1477"/>
        <source>Escape quotes</source>
        <translation>Sobrepasar comillas</translation>
    </message>
    <message>
        <source>Make output Babel compatible.</source>
        <translation type="vanished">Hacer que la salida se compatible con Babel.</translation>
    </message>
    <message>
        <source>Babel compatibility</source>
        <translation type="obsolete">Compatibilidad con Babel</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1504"/>
        <source>Replace default symbols (brackets, tilde) by nice redefinitions.</source>
        <translation>Reemplazar símbolos por defecto (llaves, corchetes, tildes) por redefiniciones apropiadas.</translation>
    </message>
    <message>
        <source>Pretty symbols</source>
        <translation type="obsolete">Símbolos lindos</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1524"/>
        <source>Embed style (defs)</source>
        <translation>Incrustar estilo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1549"/>
        <source>Name of the referenced  style file.</source>
        <translation>Nombre del archivo de estilo citado.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1552"/>
        <location filename="mainwindow.ui" line="1660"/>
        <source>highlight.sty</source>
        <translation></translation>
    </message>
    <message>
        <source>Style include:</source>
        <translation type="obsolete">Incluir estilo:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1029"/>
        <source>Set encoding:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="274"/>
        <source>Output destination:</source>
        <translation>Destino de salida</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1255"/>
        <location filename="mainwindow.ui" line="1533"/>
        <location filename="mainwindow.ui" line="1641"/>
        <location filename="mainwindow.ui" line="1936"/>
        <source>Stylesheet file:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1288"/>
        <location filename="mainwindow.ui" line="1563"/>
        <location filename="mainwindow.ui" line="1671"/>
        <location filename="mainwindow.ui" line="1966"/>
        <source>Stylesheet include file:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="17"/>
        <source>Highlight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="175"/>
        <source>Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="436"/>
        <source>Paste clipboard content into the preview window.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="439"/>
        <source>Paste from clipboard (%1)</source>
        <oldsource>Paste from clipboard</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="456"/>
        <source>Copy highlighted code into the clipboard.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="420"/>
        <source>Select syntax:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="110"/>
        <source>Wi&amp;ndows</source>
        <oldsource>&amp;Windows</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="130"/>
        <source>H&amp;ighlighting options</source>
        <oldsource>Highlighting options</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="330"/>
        <source>Browse output directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="427"/>
        <source>Select the correct syntax of the code snippet.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="459"/>
        <source>Copy preview to clipboard (%1)</source>
        <oldsource>Copy preview to clipboard</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="482"/>
        <source>Paste code from clipboard and copy the output back in one step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="488"/>
        <source>Paste, Convert and Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="514"/>
        <source>Only output the selected lines of the preview.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="517"/>
        <source>Output selected lines only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="542"/>
        <source>Plug-ins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="551"/>
        <source>Add plug-in to pool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="602"/>
        <location filename="mainwindow.ui" line="738"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="617"/>
        <source>Plug-In description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="645"/>
        <source>Plug-in parameter</source>
        <oldsource>Plug-in input file</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="654"/>
        <source>Optional plug-in parameter, this may be a path to a plug-in input file</source>
        <oldsource>Optional path to a plug-in input file</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="667"/>
        <source>Select the plug-in input file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="684"/>
        <source>Scripts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="690"/>
        <source>Choose a custom syntax or theme script.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="693"/>
        <source>Add custom script to pool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="707"/>
        <source>You can check one file of each type (syntax and theme) to override the default behaviour. If checked, the scripts will be watched for changes.</source>
        <oldsource>You can check one file of each type (syntax and theme) to override the default behaviour.</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="716"/>
        <source>Remove the selected scripts.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="726"/>
        <source>Remove all scripts.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="747"/>
        <source>Script description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="870"/>
        <source>BBCode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="875"/>
        <source>ANSI ESC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="880"/>
        <source>XTerm256 ESC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="885"/>
        <source>Truecolor ESC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="927"/>
        <source>Set line numbering start.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="993"/>
        <source>Output any plug-in text injections even if document header and footer is omitted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="996"/>
        <source>Keep Plug-In injections</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1014"/>
        <source>Generate output without version information comment.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1017"/>
        <source>Omit version info comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1026"/>
        <source>Set the output file encoding.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1187"/>
        <source>Stylesheets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1372"/>
        <source>Line numbering options:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1379"/>
        <source>Add an anchor to each line.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1402"/>
        <source>Output as ordered list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1429"/>
        <source>Enclose in pre tags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1443"/>
        <source>Add HTML MIME Type when copying code to the clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1446"/>
        <source>Copy with MIME type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1484"/>
        <source>Adapt output for the Babel package</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1487"/>
        <source>Add Babel compatibility</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1494"/>
        <source>Adapt output for the Beamer package</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1497"/>
        <source>Add Beamer compatibility</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1507"/>
        <source>Add pretty symbols</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1587"/>
        <location filename="mainwindow.ui" line="1695"/>
        <source>Path of the style include file.</source>
        <translation>Ruta del archivo de estilo a incluir.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1600"/>
        <location filename="mainwindow.ui" line="1708"/>
        <location filename="mainwindow.ui" line="2003"/>
        <source>Select a style include file.</source>
        <translation>Seleccione un archivo de estilo a incluir.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1748"/>
        <source>Set page color attribute to background color.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2150"/>
        <source>Reformat and indent your code.
This feature is enabled for C, C++, C# and Java code.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2370"/>
        <source>Font si&amp;ze:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2481"/>
        <source>&amp;Plug-Ins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2486"/>
        <source>&amp;File access trace (W32)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>TeX options</source>
        <translation type="obsolete">Opciones de TeX</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1632"/>
        <source>Embed style (macros)</source>
        <translation>Estilo de incrustación (macros)</translation>
    </message>
    <message>
        <source>RTF options</source>
        <translation type="obsolete">Opciones de RTF</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1737"/>
        <source>Add character stylesheets with formatting information.
You can select the stylesheets in your word processor to reformat additional text.</source>
        <translation>Agregar hojas de estilo de caracteres con informacion de formato.
Puede selecionar la hoja de estilo en su procesador de textos para dar formato nuevamente a texto adicional.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1741"/>
        <source>Add character styles</source>
        <translation>Agregar estilos de caracteres</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1760"/>
        <source>Page size:</source>
        <translation>Tamaño de paǵina:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1773"/>
        <source>Select a page size.</source>
        <translation>Seleccionar un tamaño de página.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1780"/>
        <source>A3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1785"/>
        <source>A4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1790"/>
        <source>A5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1795"/>
        <source>B4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1800"/>
        <source>B5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1805"/>
        <source>B6</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1810"/>
        <source>Letter</source>
        <translation>Carta</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1815"/>
        <source>Legal</source>
        <translation>Oficio</translation>
    </message>
    <message>
        <source>SVG options</source>
        <translation type="obsolete">Opciones de SVG</translation>
    </message>
    <message>
        <source>Image size:</source>
        <translation type="vanished">Tamaño de la imagen:</translation>
    </message>
    <message>
        <source>Width:</source>
        <translation type="vanished">Ancho:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1872"/>
        <source>Enter the SVG width (may contain units).</source>
        <translation>Introducir el ancho de SVG (puede incluir unidades).</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1892"/>
        <source>Height:</source>
        <translation>Alto:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1908"/>
        <source>Enter the SVG height (may contain units).</source>
        <translation>Introducir el alto de SVG (puede incluir unidades).</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2032"/>
        <source>No options defined.</source>
        <translation>No hay opciones definidas.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2057"/>
        <source>Formatting</source>
        <translation>Formato</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2086"/>
        <source>Color theme:</source>
        <translation>Tema de color:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2133"/>
        <source>Select a colour theme.</source>
        <translation>Seleccionar un tema de color.</translation>
    </message>
    <message>
        <source>Reformat and indent your code.
This feature is enabled tor C, C++, C# and Java code.</source>
        <translation type="vanished">Dar formato nuevamente e indentar su código.
Esta propiedad esta habilitada para código en C, C++, C# y Java.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2154"/>
        <source>Reformat:</source>
        <translation>Dar formato nuevamente:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2173"/>
        <source>Choose a formatting scheme.</source>
        <translation>Elija un esquema de formato.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2187"/>
        <source>Change the keyword case.</source>
        <translation>Cambiar mayúsculas/minúsculas en palabras reservadas.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2190"/>
        <source>Keyword case:</source>
        <translation>Mayúsculas/minúsculas de palabras reservadas:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2209"/>
        <source>Select a keyword case.</source>
        <translation>Seleccione un estilo de mayúsculas/minúsculas.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2216"/>
        <source>UPPER</source>
        <translation>MAYÚSCULAS</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2221"/>
        <source>lower</source>
        <translation>minúsculas</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2226"/>
        <source>Capitalize</source>
        <translation>Primera Letra Mayúscula</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2244"/>
        <source>Tab width:</source>
        <translation>Ancho de tabulador:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2260"/>
        <source>Enter the number of spaces which replace a tab.
Set the width to 0 to keep tabs.</source>
        <translation>Introducir el número de espacios que reemplazaran un tabulador.
En caso de ser 0 se mantendrán los tabuladores.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2275"/>
        <source>Enable line wrapping.</source>
        <translation>Habilitar romper lineas.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2278"/>
        <source>Line wrapping</source>
        <translation>Romper lineas</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2291"/>
        <source>Enter the maximum line length.</source>
        <translation>Introducir longitud máxima de linea.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2310"/>
        <source>Indent statements and function parameters after wrapping.</source>
        <translation>Indentar instrucciones y parámetros de funciones después de romper la linea.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2313"/>
        <source>Intelligent wrapping</source>
        <translation>Rompido inteligente</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2334"/>
        <source>Font na&amp;me:</source>
        <oldsource>Font name:</oldsource>
        <translation type="unfinished">Nombre de la fuente:</translation>
    </message>
    <message>
        <source>Select or enter the font name.</source>
        <translation type="obsolete">Seleccionar o introduzcir el nombre de la fuente.</translation>
    </message>
    <message>
        <source>Font size:</source>
        <translation type="vanished">Tamaño de la fuente:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2392"/>
        <source>Enter the font size.</source>
        <translation>Introducir el tamaño de la fuente.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2465"/>
        <source>&amp;Visit andre-simon.de</source>
        <oldsource>Visit andre-simon.de</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2476"/>
        <source>&amp;Dock floating panels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="30"/>
        <source>Preview</source>
        <translation>Previsualización</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="384"/>
        <source>Start the conversion of your input files.</source>
        <translation>Comenzar la conversión de los archivos de entrada.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="387"/>
        <source>Convert files</source>
        <translation>Convertir archivos</translation>
    </message>
    <message>
        <source>Copy highlighted code of the seleted file into the clipboard.</source>
        <translation type="vanished">Copiar codigo resaltado del archivo seleccionado al portapapeles.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="412"/>
        <source>Clipboard</source>
        <translation>Portapapeles</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="400"/>
        <source>Copy file to clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="548"/>
        <source>Choose a plug-in script.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="562"/>
        <source>List of plug-ins. Toggle checkbox to enable the scripts. The preview window may not display all plug-in effects.</source>
        <oldsource>List of plug-ins. Toggle checkbox to enable the scripts.</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="580"/>
        <source>Remove the selected plug-ins.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="590"/>
        <source>Remove all plug-ins.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="58"/>
        <source>Output progress:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="86"/>
        <source>&amp;File</source>
        <translation>&amp;Archivo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="94"/>
        <source>&amp;Help</source>
        <translation>Ay&amp;uda</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="397"/>
        <source>Copy highlighted code of the selected file into the clipboard.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="614"/>
        <source>Shows description of a selected plug-in script.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="744"/>
        <source>Shows description of a selected script.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1751"/>
        <source>Set page color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1844"/>
        <source>I&amp;mage width:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2320"/>
        <source>Do not add line numbering to lines which were automatically wrapped.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2323"/>
        <source>Omit line numbers of wrapped lines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2355"/>
        <source>Select or enter the font name. HTML supports a list of fonts, separated with comma.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2407"/>
        <source>&amp;Open files</source>
        <translation>Ab&amp;rir archivos</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2412"/>
        <source>&amp;Exit</source>
        <translation>&amp;Salir</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2417"/>
        <source>&amp;Load</source>
        <translation>&amp;Cargar</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2422"/>
        <source>&amp;Save</source>
        <translation>&amp;Guardar</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2427"/>
        <source>Load &amp;default project</source>
        <translation>Cargar proyecto por &amp;defecto</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2432"/>
        <source>&amp;Readme</source>
        <oldsource>Readme</oldsource>
        <translation type="unfinished">Leeme</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2437"/>
        <source>&amp;Tips</source>
        <translation>A&amp;visos</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2445"/>
        <source>&amp;Changelog</source>
        <translation>&amp;Bitácora de cambios</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2450"/>
        <source>&amp;License</source>
        <translation>&amp;Licencia</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2455"/>
        <source>&amp;About Highlight</source>
        <translation>&amp;Acerca de Highlight</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2460"/>
        <source>A&amp;bout translations</source>
        <oldsource>About &amp;translations</oldsource>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShowTextFile</name>
    <message>
        <location filename="showtextfile.ui" line="17"/>
        <source>Show text</source>
        <translation>Mostrar texto</translation>
    </message>
    <message>
        <location filename="showtextfile.ui" line="37"/>
        <source>TextLabel</source>
        <translation>EtiquetaDeTexto</translation>
    </message>
    <message>
        <location filename="showtextfile.ui" line="71"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>io_report</name>
    <message>
        <location filename="io_report.ui" line="14"/>
        <source>Error summary</source>
        <translation>Resumen de Error</translation>
    </message>
    <message>
        <location filename="io_report.ui" line="24"/>
        <source>Input errors:</source>
        <translation>Errores de entrada:</translation>
    </message>
    <message>
        <location filename="io_report.ui" line="34"/>
        <source>Remove files above from input file list</source>
        <translation>Remover archivos mencionados de la lista de archivos de entrada</translation>
    </message>
    <message>
        <location filename="io_report.ui" line="48"/>
        <source>Output errors:</source>
        <translation>Errores de salida:</translation>
    </message>
    <message>
        <location filename="io_report.ui" line="65"/>
        <source>Reformatting not possible:</source>
        <translation>No es posible dar formato nuevamente:</translation>
    </message>
    <message>
        <location filename="io_report.ui" line="82"/>
        <source>Failed syntax tests:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>syntax_chooser</name>
    <message>
        <location filename="syntax_chooser.ui" line="14"/>
        <source>Syntax selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="syntax_chooser.ui" line="32"/>
        <source>Select correct syntax</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="syntax_chooser.ui" line="50"/>
        <source>TextLabel</source>
        <translation type="unfinished">EtiquetaDeTexto</translation>
    </message>
    <message>
        <location filename="syntax_chooser.ui" line="74"/>
        <source>These entries are configured in filetypes.conf.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="syntax_chooser.ui" line="81"/>
        <source>The selection will be remembered for this session.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="syntax_chooser.ui" line="84"/>
        <source>Remember selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="syntax_chooser.cpp" line="57"/>
        <source>The file extension %1 is assigned to multiple syntax definitions.
Select the correct one:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
